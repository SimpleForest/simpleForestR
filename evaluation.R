#****************************************************************************
#  
# Copyright (C) 2017-2020 Dr. Jan Hackenberg, free software developer
# All rights reserved.
#
# Contact : https://gitlab.com/simpleForest
#
# Developers : Dr. Jan Hackenberg
#
# This file is developed for SimpleForest plugin Version 1 for Computree.
#
# SimpleForest plugin is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# SimpleForest plugin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.
#
# SimpleForest is an extended version of the SimpleTree platform.
#
# *****************************************************************************/
# If you use this script, please cite:
# SimpleTree —An Efficient Open Source Tool to Build Tree Models from TLS Clouds 
# I work on a more updated peer review paper and as soon I have a dedicated
# SimpleForest publication please rather cite the updated one. Not done yet though
# *****************************************************************************/

library('DescTools')

####################################################################################################
#You need to adapt the following two paths                                       ###################
####################################################################################################
path = '/home/drsnuggles/Documents/R/detailed/'
#path = '/home/drsnuggles/clouds/evaluation9/sphereGV/qsm/detailed/'
#path = '/home/drsnuggles/clouds/evaluationa/qsm/detailed/'
groundTruth <- read.csv('/home/drsnuggles/Documents/clouds/all_data/gt/GT_36b.csv', header = FALSE)

####################################################################################################
#Some string formatting for the field data                                       ###################
####################################################################################################
groundTruth$V1 <- as.character(groundTruth$V1)
croppNamesGroundTruth <- substr(groundTruth$V1,1, nchar(groundTruth$V1)-4)
groundTruth$V1 <- croppNamesGroundTruth
fileList <- list.files(path)
croppedFileList <- substr(fileList,1,nchar(fileList)-4)

####################################################################################################
#Create output vectors                                                           ###################
####################################################################################################
measuredVol <-  c()
expandedVol <- c()
GTVol <- c()      
namesa <- c()

####################################################################################################
#Parse QSMs and field csv to retrieve linkable volume in output                  ###################
####################################################################################################
index = 1
a <- 1
for(file in fileList)
{
  fullPath <- paste(path,file, sep = "")
  qsm <- read.csv(fullPath)
  names <- rep(croppedFileList[index], length(qsm$type))
  qsm$name <- names
  gt <- groundTruth[which (groundTruth$V1 == croppedFileList[index]),]
    if(nrow(gt)==1)
    {
      GTVol <- c(GTVol, gt$V4)
      vol <- max(qsm$growthVolume)
      partialQSM <- qsm[which(qsm$radius > 0.05),]
      vol2 <- sum(partialQSM$volume)
      if(substring(qsm$name[1],1,1)=="Q")
      {
        vol2 = vol2*1.14;
      }
      if(substring(qsm$name[1],1,1)=="E")
      {
        vol2 = vol2*1.37;
      }
      if(substring(qsm$name[evaluations],1,1)=="P")
      {
        vol2 = vol2*1.25;
      }
      expandedVol <- c(expandedVol,vol2)
      measuredVol<- c(measuredVol,vol)
      namesa <- c(namesa, unique(croppedFileList[index]))
    }
  index = index + 1
}

####################################################################################################
#Link field and predicted volume, do error measures                              ###################
####################################################################################################
error <- 100*(measuredVol- GTVol)/GTVol
max <- max(max(measuredVol),max(GTVol))
totalError <- 100*(sum(measuredVol)- sum(GTVol))/sum(GTVol)
cError <- CCC(measuredVol, GTVol)
summary(lm(measuredVol~GTVol))

####################################################################################################
#Plot the model                                                                  ###################
####################################################################################################
plot(GTVol, 
     measuredVol, 
     xlim = c(0,max), 
     ylim = c(0,max), 
     xlab = "Field measured Volume [m^3]",
     ylab = "Spherefollowing predicted Volume [m^3]",
     main = "Validation total Volume")
abline(lm(measuredVol~GTVol), lwd = 2, lty = 1)
lines(c(0,max),c(0,max), lwd = 2, lty = 2)
points(GTVol[1:12], measuredVol[1:12], pch = 20, col = "red")
points(GTVol[13:24], measuredVol[13:24], pch = 20, col = "green")
points(GTVol[25:36], measuredVol[25:36], pch = 20, col = "blue")

####################################################################################################
#Add legend for tree species                                                     ###################
####################################################################################################
leg.txt = c("Erythropleum fordii", "Pinus massoniana", "Quercus petraea")
legend("topleft",  leg.txt
       ,col= c("red", "green", "blue" )
       ,pch = c(20,20)
       ,cex = 1
       ,pt.cex =1
)
####################################################################################################
#Add legend for linear model and 1:1 line                                        ###################
####################################################################################################
leg.txt = c("linear model", "1:1 line")
legend("left",  leg.txt
       ,lwd = c(2)
       ,lty = c(1,2)
       ,pt.cex =1
)

####################################################################################################
#Add legend for error measures                                                   ###################
####################################################################################################
r2 <- paste("r-squared :",
            format(summary(lm(measuredVol~GTVol))$r.squared, 
            digits = 2))
total <- paste("total error :",
               format(totalError, 
               digits = 2))
ccc <- paste("CCC         :",
             format(cError$rho.c$est, 
             digits = 2))
leg.txt = c(r2, total, ccc)
legend("bottomright",  leg.txt
       ,col = c("white")
       ,pt.cex =1
)






####################################################################################################
#Link field and predicted volume, do error measures                              ###################
####################################################################################################
error <- 100*(expandedVol- GTVol)/GTVol
max <- max(max(expandedVol),max(GTVol))
totalError <- 100*(sum(expandedVol)- sum(GTVol))/sum(GTVol)
cError <- CCC(expandedVol, GTVol)
summary(lm(expandedVol~GTVol))

####################################################################################################
#Plot the model                                                                  ###################
####################################################################################################
plot(GTVol, 
     expandedVol, 
     xlim = c(0,max), 
     ylim = c(0,max), 
     xlab = "Field measured Volume [m^3]",
     ylab = "Spherefollowing predicted Volume [m^3]",
     main = "Validation BEF volume")
abline(lm(expandedVol~GTVol), lwd = 2, lty = 1)
lines(c(0,max),c(0,max), lwd = 2, lty = 2)
points(GTVol[1:12], expandedVol[1:12], pch = 20, col = "red")
points(GTVol[13:24], expandedVol[13:24], pch = 20, col = "green")
points(GTVol[25:36], expandedVol[25:36], pch = 20, col = "blue")

####################################################################################################
#Add legend for tree species                                                     ###################
####################################################################################################
leg.txt = c("Erythropleum fordii", "Pinus massoniana", "Quercus petraea")
legend("topleft",  leg.txt
       ,col= c("red", "green", "blue" )
       ,pch = c(20,20)
       ,cex = 1
       ,pt.cex =1
)
####################################################################################################
#Add legend for linear model and 1:1 line                                        ###################
####################################################################################################
leg.txt = c("linear model", "1:1 line")
legend("left",  leg.txt
       ,lwd = c(2)
       ,lty = c(1,2)
       ,pt.cex =1
)

####################################################################################################
#Add legend for error measures                                                   ###################
####################################################################################################
r2 <- paste("r-squared :",
            format(summary(lm(expandedVol~GTVol))$r.squared, 
                   digits = 2))
total <- paste("total error :",
               format(totalError, 
                      digits = 2))
ccc <- paste("CCC         :",
             format(cError$rho.c$est, 
                    digits = 2))
leg.txt = c(r2, total, ccc)
legend("bottomright",  leg.txt
       ,col = c("white")
       ,pt.cex =1
)
